#include "threads.h"

void I_Love_Threads()
{
	std::cout << "I Love Threads" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.join();
}


void printVector(std::vector<int> primes)
{
	for (std::vector<int>::iterator i = primes.begin(); i != primes.end(); ++i)
		std::cout << *i << std::endl;
}

bool isPrime(int n, int i = 2)
{
	if (n <= 1)
		return false;
	if (i * i > n) { // until sqrt of the number
		return true;
	}
	if (n % i == 0) {
		return false;
	}

	return isPrime(n, i + 1);
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	// pushing prime numbers to the vector
	for (int i = begin; i <= end; i++) {
		if (isPrime(i, 2)) {
			primes.push_back(i);
		}
	}
}

std::vector<int> callGetPrimes(int begin, int end)
{
	std::vector<int> primes;
	const auto before = std::chrono::system_clock::now(); // starting the timer
	std::thread t1(getPrimes, begin, end, std::ref(primes)); // creating thread

	t1.join(); // waiting for the thread to end

	const std::chrono::duration<double> duration = std::chrono::system_clock::now() - before; // calculating time

	std::cout << "The calculation took: " << duration.count() << " seconds" << std::endl;

	return primes;
}



void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	// writing prime numbers in the file
	for (int i = begin; i <= end; i++) {
		if (isPrime(i, 2)) {
			file << (std::to_string(i) + "\n");
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int range = (end - begin) / N; // the range of numbers each thread will work with

	std::vector<std::thread> thVec;
	std::ofstream file(filePath);
	if (file.is_open())
	{
		const auto before = std::chrono::system_clock::now(); // starting the timer

		for (int i = 0; i < N; i++) {	// pushing threads to vector
			begin += range; // each thread gets a different range to work with
			std::thread t(writePrimesToFile, begin - range, begin, std::ref(file));
			thVec.push_back(std::move(t));
		}

		for (int i = 0; i < N; i++) { // waiting until all threads are finished
			thVec[i].join();
		}

		const std::chrono::duration<double> duration = std::chrono::system_clock::now() - before; // calculating time
		std::cout << "The calculation took: " << duration.count() << " seconds" << std::endl;
	}
	else std::cout << "Unable to open file";
}


void colorfulThreads()
{
	const std::string colors[NUM_OF_COLORS] = {
	BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA,
	CYAN, WHITE, BOLDWHITE, BOLDRED, BOLDGREEN, BOLDYELLOW,
	BOLDBLUE, BOLDMAGENTA, BOLDCYAN};

	std::vector<std::thread> thVec;

	for (std::string color : colors) { // pushing threads with different colors to vector
		std::thread t(printColorfulMsg, color);
		thVec.push_back(std::move(t));
	}

	for (int i = 0; i < NUM_OF_COLORS; i++) { // waiting for all threads to finish
		thVec[i].join();
	}
}

void printColorfulMsg(std::string colorCode)
{
	for (int i = 0; i < 10; i++) {
		std::cout << (colorCode + "color" + RESET + '\n');
	}
}

void maxValue()
{
	int arr[ARR_LENGTH];
	int range = ARR_LENGTH / NUM_OF_THREADS, pos = 0, maxVal = 0, temp = 0;
	std::vector<std::thread> thVec;

	srand(time(NULL));

	for (int i = 0; i < ARR_LENGTH; i++) { // filling the array with random numbers
		arr[i] = rand();
	}

	for (int i = 0; i < NUM_OF_THREADS; i++) { // pushing 10 threads to the vector
		pos += range;						   // each thread works with another range of numbers
		std::thread t(getMaxValue, (pos - range), pos, arr, std::ref(temp));
		thVec.push_back(std::move(t));
	}

	for (int i = 0; i < NUM_OF_THREADS; i++) { // waiting for all threads to finish
		thVec[i].join(); 
		maxVal = std::max(maxVal, temp); // getting max value from the thread that just finished
	}

	std::cout << "The element with max value: " << maxVal << std::endl;
}

void getMaxValue(int begin, int end, int* arr, int& maxVal)
{
	int max = arr[begin];
	for (int i = begin; i < end; i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	maxVal = max; // max value returns in parameters
}
