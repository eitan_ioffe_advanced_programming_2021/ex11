#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>

#define NUM_OF_COLORS 15
#define ARR_LENGTH 1000
#define NUM_OF_THREADS 10

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

void I_Love_Threads();
void call_I_Love_Threads();

void printVector(std::vector<int> primes); // printing vector's values

bool isPrime(int n, int i); // returning if the number is prime
void getPrimes(int begin, int end, std::vector<int>& primes); // putting all prime numbers in the given range in the vector
std::vector<int> callGetPrimes(int begin, int end); // function to call a thread of getPrimes and calculate the time it took


void writePrimesToFile(int begin, int end, std::ofstream& file); // writing all prime numbers in the range given into the file
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N); // calling N number of writePrimesToFile threads

// Bonus functions:
void colorfulThreads();
void printColorfulMsg(std::string colorCode);

void maxValue();
void getMaxValue(int begin, int end, int* arr, int& maxVal);
