#include "threads.h"

int main()
{
	call_I_Love_Threads();

	std::vector<int> primes3 = callGetPrimes(0, 1000);
	primes3 = callGetPrimes(0, 100000);
	primes3 = callGetPrimes(0, 1000000);  // should take 2 seconds

	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 1);
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 10);


	colorfulThreads();

	maxValue();

	system("pause");
	return 0;
}